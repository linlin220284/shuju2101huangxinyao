import pytest
import allure
from selenium import webdriver

# 前置条件：使用 Chrome 浏览器打开百度网站
@pytest.fixture(scope="session")
def browser():
    driver = webdriver.Chrome()
    driver.get("https://www.baidu.com")
    yield driver
    driver.quit()

# 用例标题：测试百度搜索功能
@allure.title("测试百度搜索功能")
# 用例步骤：输入搜索关键字并点击搜索按钮
def test_search(browser):
    # 输入搜索关键字
    search_input = browser.find_element_by_id("kw")
    search_input.send_keys("Python")
    # 点击搜索按钮
    search_button = browser.find_element_by_id("su")
    search_button.click()
    # 预期结果：搜索结果包含关键字 Python
    expected_result = "Python"
    # 实际结果：获取搜索结果中的第一条结果的标题，并判断是否包含关键字 Python
    actual_result = browser.find_element_by_css_selector(".result-title a").text
    assert expected_result in actual_result

# 用例标题：测试搜索结果页数
@allure.title("测试搜索结果页数")
# 用例步骤：输入搜索关键字并点击搜索按钮，获取搜索结果页数
def test_page_number(browser):
    # 输入搜索关键字并点击搜索按钮
    search_input = browser.find_element_by_id("kw")
    search_input.send_keys("Python")
    search_button = browser.find_element_by_id("su")
    search_button.click()
    # 预期结果：搜索结果页数大于等于 1
    expected_result = 1
    # 实际结果：获取搜索结果页数，并判断是否大于等于 1
    actual_result = int(browser.find_element_by_css_selector(".page-number span:last-child").text)
    assert actual_result >= expected_result
